package main

import (
	"github.com/rodrigoberna1996/guros/api"
	"github.com/rodrigoberna1996/guros/domain/services"
	"github.com/rodrigoberna1996/guros/infrastructure/db"
	"github.com/rodrigoberna1996/guros/infrastructure/repositories"
	"log"
	"os"
)

func main() {
	//No necesario para el despliegue en GCP
	/*if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}*/

	dbURI := os.Getenv("MONGODB_URI")
	if dbURI == "" {
		log.Fatalf("MONGODB_URI not set in .env file")
	}

	dbClient, err := db.InitMongoDB(dbURI)
	if err != nil {
		log.Fatalf("Error initializing database: %v", err)
	}
	defer dbClient.Disconnect(nil)

	// Crea una instancia de tu repositorio
	collection := dbClient.Database("guros").Collection("mutations")
	adnRepo := repositories.NewADNRepository(collection)

	// Crea una instancia del servicio DNAservice
	adnService := services.NewADNService(adnRepo)

	// Inicia la API pasando el servicio como argumento
	api.Start(adnService, adnRepo)
}
