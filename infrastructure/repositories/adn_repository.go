package repositories

import (
	"context"
	"github.com/rodrigoberna1996/guros/domain/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type ADNRepository interface {
	Save(adn models.ADN) (*mongo.InsertOneResult, error)
	Find(sequence []string) (*models.ADN, error)
	GetStats() (*models.ADNStats, error)
}

type mongoADNRepository struct {
	collection *mongo.Collection
}

func NewADNRepository(collection *mongo.Collection) ADNRepository {
	return &mongoADNRepository{collection: collection}
}

func (r *mongoADNRepository) Save(adn models.ADN) (*mongo.InsertOneResult, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	return r.collection.InsertOne(ctx, adn)
}

func (r *mongoADNRepository) Find(sequence []string) (*models.ADN, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.M{"sequence": sequence}
	var adn models.ADN

	err := r.collection.FindOne(ctx, filter).Decode(&adn)
	if err == mongo.ErrNoDocuments {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &adn, nil
}

func (r *mongoADNRepository) GetStats() (*models.ADNStats, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	pipeline := mongo.Pipeline{
		{{"$group", bson.D{
			{"_id", "$hasMutation"},
			{"count", bson.D{{"$sum", 1}}},
		}}},
	}

	cursor, err := r.collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	var countMutations, countNoMutations int64
	for cursor.Next(ctx) {
		var result struct {
			ID    bool `bson:"_id"`
			Count int64
		}
		if err := cursor.Decode(&result); err != nil {
			return nil, err
		}

		if result.ID {
			countMutations = result.Count
		} else {
			countNoMutations = result.Count
		}
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	ratio := float64(countMutations) / float64(countNoMutations+countMutations)

	return &models.ADNStats{
		CountMutations:  int(countMutations),
		CountNoMutation: int(countNoMutations),
		Ratio:           ratio,
	}, nil
}
