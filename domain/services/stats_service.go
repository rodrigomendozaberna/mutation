// domain/services/stats_service.go

package services

import (
	"github.com/rodrigoberna1996/guros/domain/models"
	"github.com/rodrigoberna1996/guros/infrastructure/repositories"
)

type StatsService struct {
	adnRepo repositories.ADNRepository
}

func NewStatsService(repo repositories.ADNRepository) *StatsService {
	return &StatsService{
		adnRepo: repo,
	}
}

func (s *StatsService) GetStatistics() (*models.ADNStats, error) {
	return s.adnRepo.GetStats()
}
