package services

import (
	"github.com/rodrigoberna1996/guros/domain/models"
	"github.com/rodrigoberna1996/guros/infrastructure/repositories"
)

type ADNService struct {
	adnRepo repositories.ADNRepository
}

func NewADNService(repo repositories.ADNRepository) *ADNService {
	return &ADNService{adnRepo: repo}
}

func (s *ADNService) CheckAndStoreADN(adn []string) (bool, error) {
	existingADN, err := s.adnRepo.Find(adn)
	if err != nil {
		return false, err
	}

	if existingADN != nil {
		return existingADN.HasMutation, nil
	}

	hasMutation := s.HasMutation(adn)

	newADN := models.ADN{
		Sequence:    adn,
		HasMutation: hasMutation,
	}
	_, err = s.adnRepo.Save(newADN)
	if err != nil {
		return false, err
	}

	return hasMutation, nil
}

func (s *ADNService) HasMutation(dna []string) bool {
	n := len(dna)
	count := 0

	isValid := func(x, y int) bool {
		return x >= 0 && x < n && y >= 0 && y < n
	}

	directions := [][]int{
		{0, 1}, // Horizontal
		{1, 0}, // Vertical
		{1, 1}, // Diagonal
	}

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			for _, dir := range directions {
				dx, dy := dir[0], dir[1]

				if isValid(i+3*dx, j+3*dy) &&
					dna[i][j] == dna[i+dx][j+dy] &&
					dna[i][j] == dna[i+2*dx][j+2*dy] &&
					dna[i][j] == dna[i+3*dx][j+3*dy] {
					count++
					if count == 2 {
						return true
					}
				}
			}
		}
	}
	return false
}
