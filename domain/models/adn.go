package models

type ADN struct {
	Sequence    []string `bson:"sequence"`
	HasMutation bool     `bson:"hasMutation"`
}

type ADNCheckRequest struct {
	ADN []string `json:"adn"`
}
