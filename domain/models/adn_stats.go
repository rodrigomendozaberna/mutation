package models

type ADNStats struct {
	CountMutations  int     `json:"count_mutations"`
	CountNoMutation int     `json:"count_no_mutation"`
	Ratio           float64 `json:"ratio"`
}
