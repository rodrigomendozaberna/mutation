package api

import (
	"github.com/gin-gonic/gin"
	"github.com/rodrigoberna1996/guros/api/handlers"
	"github.com/rodrigoberna1996/guros/api/middlewares"
	"github.com/rodrigoberna1996/guros/domain/services"
	"github.com/rodrigoberna1996/guros/infrastructure/repositories"
)

func Start(adnService *services.ADNService, adnRepo repositories.ADNRepository) {
	router := gin.Default()

	mutationHandler := handlers.NewADNHandler(adnService)

	statsService := services.NewStatsService(adnRepo)
	statsHandler := handlers.NewStatsHandler(statsService)

	router.POST("/oauth/token", handlers.TokenHandler)
	router.GET("/stats", statsHandler.GetStats)

	authorized := router.Group("/")
	authorized.Use(middlewares.JwtMiddleware())
	{
		authorized.POST("/mutation", mutationHandler.HandleADN)
	}

	err := router.Run(":8080")
	if err != nil {
		return
	}
}
