package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/rodrigoberna1996/guros/domain/services"
	"net/http"
)

type StatsHandler struct {
	statsService *services.StatsService
}

func NewStatsHandler(service *services.StatsService) *StatsHandler {
	return &StatsHandler{statsService: service}
}

func (h *StatsHandler) GetStats(c *gin.Context) {
	stats, err := h.statsService.GetStatistics()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get statistics"})
		return
	}

	c.JSON(http.StatusOK, stats)
}
