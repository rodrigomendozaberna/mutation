package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/rodrigoberna1996/guros/domain/authenticator"
	"github.com/rodrigoberna1996/guros/domain/models"
	"net/http"
	"os"
)

func TokenHandler(c *gin.Context) {
	var credentials models.Credentials

	if err := c.BindJSON(&credentials); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	expectedClientID := os.Getenv("EXPECTED_CLIENT_ID")
	expectedClientSecret := os.Getenv("EXPECTED_CLIENT_SECRET")

	if credentials.ClientID != expectedClientID || credentials.ClientSecret != expectedClientSecret {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid credentials"})
		return
	}

	userID := credentials.ClientID
	token, err := authenticator.GenerateJWT(userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error generating token"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"access_token": token, "token_type": "bearer"})
}
