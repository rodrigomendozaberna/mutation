package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rodrigoberna1996/guros/domain/models"
	"github.com/rodrigoberna1996/guros/domain/services"
	"net/http"
)

type ADNHandler struct {
	adnService *services.ADNService
}

func NewADNHandler(service *services.ADNService) *ADNHandler {
	return &ADNHandler{adnService: service}
}

func (h *ADNHandler) HandleADN(c *gin.Context) {
	var adnRequest models.ADNCheckRequest

	if err := c.BindJSON(&adnRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	fmt.Println("CheckAndStoreADN", adnRequest)
	hasMutation, err := h.adnService.CheckAndStoreADN(adnRequest.ADN)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to process ADN"})
		return
	}

	fmt.Println("hasMutation", hasMutation)

	if hasMutation {
		c.Status(http.StatusOK)
	} else {
		c.Status(http.StatusForbidden)
	}
}
