# Mutación API

Este proyecto es una API que permite verificar las mutaciones en secuencias de ADN.

## Índice

- [Requisitos previos](#requisitos-previos)
- [Configuración del entorno](#configuración-del-entorno)
- [Ejecución del proyecto](#ejecución-del-proyecto)
- [Endpoints](#endpoints)

## Requisitos previos

- [Go](https://golang.org/doc/install) (Versión recomendada: 1.17+)
- [MongoDB](https://docs.mongodb.com/manual/installation/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Docker](https://docs.docker.com/get-docker/) (Opcional, si decides usar contenedores)

## Configuración del entorno

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/rodrigomendozaberna/mutation.git
   cd mutation

2. **Configura las variables de entorno:**

   Copia el archivo .env.example a .env y completa las variables necesarias:

   ```bash
   cp .env.example .env
   ```
   Edita el archivo .env con tu editor favorito y configura las variables.


3. **(Opcional) Docker:**

   Si optas por usar Docker, puedes construir y ejecutar el contenedor con los siguientes comandos:

   ```bash
   docker build -t guros .
   docker run -p 8080:8080 guros
   ```

## Ejecución del proyecto

Para ejecutar el proyecto localmente:

   ```bash
   go run main.go
   ```

## Endpoints

* POST /oauth/token: Genera un token para autenticar las peticiones
* POST /mutation: Verifica una secuencia de ADN.
* GET /stats: Devuelve estadísticas de verificaciones de ADN.
